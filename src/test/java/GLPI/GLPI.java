package GLPI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;



public class GLPI {
	public static ChromeDriver driver;
	
	@BeforeClass
	public static void steupClass() {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://demo.glpi-project.org/index.php");
	}

	@Test
	@Ignore
	public void a_ConnexionGLPI()
	{
		assertEquals("GLPI - Authentication", driver.getTitle());
		assertTrue(driver.findElement(By.id("login_remember")).isSelected());
		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");
		new Select(driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		driver.findElement(By.name("submit")).click();
		driver.findElement(By.id("preferences_link")).click();
		assertTrue(driver.findElement(By.tagName("body")).getText().contains("admin"));
		driver.findElement(By.id("deconnexion")).click();
	}
	
	@Test
	@Ignore
	public void b_LangueDuProfil() 
	{
		int nbLangues =  new Select(driver.findElement(By.name("language"))).getOptions().size();
		for (int i = 1; i < nbLangues; i++)
		{
			String langue = new Select(driver.findElement(By.name("language"))).getOptions().get(i).getText();
			driver.findElement(By.id("login_name")).sendKeys("admin");
			driver.findElement(By.id("login_password")).sendKeys("admin");
			new Select(driver.findElement(By.name("language"))).selectByVisibleText(langue);
			driver.findElement(By.name("submit")).click();
			driver.findElement(By.id("deconnexion")).click();
		}
	}
	
	@Test
	public void c_AjouterOrdinateur()
	{
		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");
		//new Select(driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		
		driver.findElement(By.xpath("//select[@name='language']/parent::span")).click();
		driver.findElement(By.xpath("//span[contains(.,'Fran�ais')]"));
		
		driver.findElement(By.name("submit")).click();
		driver.findElement(By.id("menu_all_button")).click();
		driver.findElement(By.linkText("Ordinateurs")).click();
		driver.findElement(By.cssSelector("i.fa.fa-plus")).click();
		driver.findElement(By.linkText("Gabarit vide")).click();
		driver.findElement(By.name("name")).sendKeys("Toshib0");
		
		driver.findElement(By.xpath("//select[@name='users_id_tech']/parent::td")).click();
		driver.findElement(By.xpath("//[contains(.,'King Eden')]")).click();
		
		driver.findElement(By.xpath("//select[@name='domains_id']/parent::span")).click();
		driver.findElement(By.xpath("//li[contains(.,'IUT86')]")).click();
		
		driver.findElement(By.xpath("//select[@name='networks_id']/parent::span")).click();
		driver.findElement(By.xpath("//li[contains(.,'SIC')]")).click();
		
		driver.findElement(By.xpath("//select[@name='computermodels_id']/parent::span")).click();
		driver.findElement(By.xpath("//li[contains(.,'LENOVO')]")).click();
		
		driver.findElement(By.xpath("//select[@name='states_id']/parent::span")).click();
		driver.findElement(By.xpath("//li[contains(.,'En fonction')]")).click();
		
		driver.findElement(By.xpath("//select[@name='manufacturers_id']/parent::span")).click();
		driver.findElement(By.xpath("//li[contains(.,'CANON')]")).click();
		
		driver.findElement(By.name("serial")).sendKeys("123456");
		driver.findElement(By.name("comment")).sendKeys("Adnane Ezzamel");
		driver.findElement(By.name("add")).click();
		driver.findElement(By.name("globalsearch")).sendKeys("Toshib0");
		driver.findElement(By.name("globalsearchglass")).click();
		assertTrue(driver.findElement(By.tagName("body")).getText().contains("Toshib0"));
		
	}
	
	@AfterClass
	public static void tearDownClass() {
		
	}
}
